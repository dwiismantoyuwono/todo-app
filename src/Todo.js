import React from "react"
import {
  Grid,
  Stack,
  Button,
  Typography,
  TextField,
  Divider,
  Checkbox,
} from "@mui/material"
import DeleteIcon from "@mui/icons-material/Delete"

const Todo = () => {
  const [list, setList] = React.useState([
    {
      id: Date.now(),
      title: "Daily meeting - morning",
      completed: false,
    },
  ])

  const [newTodo, setNewTodo] = React.useState("")

  const handleSubmit = () => {
    if (newTodo !== "") {
      setList([
        ...list,
        {
          id: Date.now(),
          title: newTodo,
          completed: false,
        },
      ]);
      setNewTodo("");
    }
  }

  const handleComplete = (id, checked) => {
    setList(
      list.map((item) =>
        item.id === id ? { ...item, completed: checked } : item
      )
    );
  }

  const handleDelete = (id) => {
    setList(list.filter((item) => item.id !== id));
  }

  return (
    <Grid container spacing={4} justifyContent="center" sx={{ py: 4 }}>
      <Grid item sm={6}>
        {/* Title */}
        <Typography fontWeight={700} fontSize={24}>
          Todo List
        </Typography>
        <Divider sx={{ mb: 4 }} />

        {/* Input */}
        <Stack direction="row" spacing={2}>
          <TextField
            label="Todo"
            fullWidth
            value={newTodo}
            onChange={(e) => setNewTodo(e.target.value)}
          />
          <Button
            variant="contained"
            disabled={!newTodo}
            onClick={handleSubmit}
          >
            Submit
          </Button>
        </Stack>
        <Divider sx={{ my: 4 }} />

        {/* List */}
        {list.map((item) => (
          <Grid key={item.id} container spacing={2} alignItems="center">
            <Grid item sm={1}>
              <Checkbox
                checked={item.completed}
                onChange={(_, checked) => {
                  handleComplete(item.id, checked)
                }}
              />
            </Grid>
            <Grid item sm={9}>
              <Typography>{item.title}</Typography>
            </Grid>
            <Grid item sm={2}>
              <Button
                variant="outlined"
                color="error"
                startIcon={<DeleteIcon />}
                onClick={() => {
                  handleDelete(item.id)
                }}
              >
                Delete
              </Button>
            </Grid>
          </Grid>
        ))}
      </Grid>
    </Grid>
  )
}

export default Todo
